import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final Widget child;

  MyApp({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  final Widget child;

  MyHomePage({Key key, this.child}) : super(key: key);

  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Center(
          child: new Text('My Frist App'),
        ),
      ),
      body: new _MyList(),
    );
  }
}

class _MyList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
      padding: const EdgeInsets.all(4.0),
      itemBuilder: (context, i) {
        return new ListTile(
          title: Text('Some Random Username'),
          subtitle: new Text('Online', style: TextStyle(fontStyle: FontStyle.italic, color: Colors.green),),
          leading: const Icon(Icons.face),
          trailing: new RaisedButton(
            child: new Text('Remove'),
            onPressed: (){
              deleteDialog(context).then((value) {
                print('Value is $value');
              });
            },
          ),
        );
      },
    );
  }
}

Future<bool> deleteDialog(BuildContext context) {
  return showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
        title: new Text('Are you sure ?'),
        actions: <Widget>[
          new FlatButton(
            child: new Text('yes'),
            onPressed: () {
              Navigator.of(context).pop(true);
            },
          ),
          new FlatButton(
            child: new Text('no'),
            onPressed: () {
              Navigator.of(context).pop(false);
            },
          ),
        ],
      );
    }
  );
}
