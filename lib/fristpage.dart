import 'package:flutter/material.dart';

class FristPage extends StatelessWidget {
  final Widget child;

  FristPage({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Center(
        child: new Text('Frist Page', style:TextStyle(fontSize: 25.0, color:Colors.purple)),
      ),
    );
  }
}