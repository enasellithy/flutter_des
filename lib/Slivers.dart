import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final Widget child;

  MyApp({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  final Widget child;

  MyHomePage({Key key, this.child}) : super(key: key);

  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: new Text('Some Title'),
      ),
      body: new CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            expandedHeight: 250.0,
            flexibleSpace: new FlexibleSpaceBar(
              background: Image.network('https//placeimg.com/480/320/any'),
            ),
          ),
          new SliverList(
            delegate: new SliverChildBuilderDelegate((context, index) =>
             new Card(
               child: new Container(
                 padding: EdgeInsets.all(10.0),
                 child: new Row(
                   mainAxisAlignment: MainAxisAlignment.start,
                   children: <Widget>[
                     CircleAvatar(
                       backgroundColor: Colors.transparent,
                       backgroundImage: new NetworkImage('https//placeimg.com/480/320/any'),
                     ),
                     Text('I am the card content !'),
                   ],
                 ),
               ),
             )),
          ),
        ],
      ),
    );
  }
}