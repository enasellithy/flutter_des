import 'package:flutter/material.dart';
import './main.dart';

class AboutPage extends StatefulWidget {
  final Widget child;

  AboutPage({Key key, this.child}) : super(key: key);

  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: InkWell(
          onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => new MyApp()));},
          child: Text('User Account'),
        ),
      ),
      drawer: new Drawer(
        child: ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountName: new Text('User', style: TextStyle(color: Colors.white,)),
              accountEmail: new Text('user@mail.com', style:TextStyle(color:Colors.white)),
              currentAccountPicture: new CircleAvatar(
                backgroundColor: Colors.grey,
                child: Icon(Icons.person, color: Colors.white,),
              ),
            ),
            // ListTile(
            //   title: InkWell(
            //     onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => new MyApp()));},
            //     child: new Text('About Page'),
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}